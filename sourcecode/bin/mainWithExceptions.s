	.file	"mainWithExceptions.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZN14WithExceptionsC2Ev,"axG",@progbits,_ZN14WithExceptionsC5Ev,comdat
	.align 2
	.weak	_ZN14WithExceptionsC2Ev
	.type	_ZN14WithExceptionsC2Ev, @function
_ZN14WithExceptionsC2Ev:
.LFB1323:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV14WithExceptions+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEEC1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	_ZN14WithExceptionsC2Ev, .-_ZN14WithExceptionsC2Ev
	.weak	_ZN14WithExceptionsC1Ev
	.set	_ZN14WithExceptionsC1Ev,_ZN14WithExceptionsC2Ev
	.section	.rodata
.LC0:
	.string	"logfile_ex.log"
.LC1:
	.string	"test_entry"
	.text
	.globl	_Z4testv
	.type	_Z4testv, @function
_Z4testv:
.LFB1321:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$608, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
.LEHB0:
	call	_ZN14WithExceptionsC1Ev
.LEHE0:
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-65(%rbp), %rdx
	leaq	-80(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB1:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE1:
	leaq	-80(%rbp), %rdx
	leaq	-624(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB2:
	call	_ZN14WithExceptions8openFileESs
.LEHE2:
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L10:
	leaq	-25(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-25(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movl	$.LC1, %esi
	movq	%rax, %rdi
.LEHB3:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE3:
	leaq	-48(%rbp), %rdx
	leaq	-624(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB4:
	call	_ZN14WithExceptions9writeFileESs
.LEHE4:
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-25(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN14WithExceptionsD1Ev
	jmp	.L19
.L14:
	movq	%rax, %r12
	movq	%rdx, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L4
.L13:
	movq	%rax, %r12
	movq	%rdx, %rbx
.L4:
	leaq	-65(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	movq	%r12, %rax
	movq	%rbx, %rdx
	cmpq	$1, %rdx
	je	.L6
	jmp	.L20
.L16:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L8
.L15:
	movq	%rax, %rbx
.L8:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	call	__cxa_end_catch
	jmp	.L9
.L20:
	movq	%rax, %rbx
	jmp	.L9
.L6:
	movq	%rax, %rdi
	call	__cxa_begin_catch
	movq	%rax, -24(%rbp)
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-49(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB5:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE5:
	leaq	-64(%rbp), %rdx
	leaq	-624(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB6:
	call	_ZN14WithExceptions10createFileESs
.LEHE6:
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	call	__cxa_end_catch
	jmp	.L10
.L18:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L12
.L17:
	movq	%rax, %rbx
.L12:
	leaq	-25(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L9:
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN14WithExceptionsD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB7:
	call	_Unwind_Resume
.LEHE7:
.L19:
	addq	$608, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1321:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
	.align 4
.LLSDA1321:
	.byte	0xff
	.byte	0x3
	.uleb128 .LLSDATT1321-.LLSDATTD1321
.LLSDATTD1321:
	.byte	0x1
	.uleb128 .LLSDACSE1321-.LLSDACSB1321
.LLSDACSB1321:
	.uleb128 .LEHB0-.LFB1321
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB1321
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L13-.LFB1321
	.uleb128 0x3
	.uleb128 .LEHB2-.LFB1321
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L14-.LFB1321
	.uleb128 0x3
	.uleb128 .LEHB3-.LFB1321
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L17-.LFB1321
	.uleb128 0
	.uleb128 .LEHB4-.LFB1321
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L18-.LFB1321
	.uleb128 0
	.uleb128 .LEHB5-.LFB1321
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L15-.LFB1321
	.uleb128 0
	.uleb128 .LEHB6-.LFB1321
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L16-.LFB1321
	.uleb128 0
	.uleb128 .LEHB7-.LFB1321
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
.LLSDACSE1321:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.align 4
	.long	_ZTISt9exception
.LLSDATT1321:
	.text
	.size	_Z4testv, .-_Z4testv
	.globl	main
	.type	main, @function
main:
.LFB1325:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z4testv
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	main, .-main
	.section	.rodata
	.align 8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
