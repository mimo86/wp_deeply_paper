	.file	"mainWithReturnValue.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZN15WithReturnValueC2Ev,"axG",@progbits,_ZN15WithReturnValueC5Ev,comdat
	.align 2
	.weak	_ZN15WithReturnValueC2Ev
	.type	_ZN15WithReturnValueC2Ev, @function
_ZN15WithReturnValueC2Ev:
.LFB1323:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV15WithReturnValue+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEEC1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	_ZN15WithReturnValueC2Ev, .-_ZN15WithReturnValueC2Ev
	.weak	_ZN15WithReturnValueC1Ev
	.set	_ZN15WithReturnValueC1Ev,_ZN15WithReturnValueC2Ev
	.section	.rodata
.LC0:
	.string	"logfile_rv.log"
.LC1:
	.string	"test_entry"
	.text
	.globl	_Z4testv
	.type	_Z4testv, @function
_Z4testv:
.LFB1321:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1321
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -24
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
.LEHB0:
	call	_ZN15WithReturnValueC1Ev
.LEHE0:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-49(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB1:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE1:
	leaq	-64(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB2:
	call	_ZN15WithReturnValue8openFileESs
.LEHE2:
	testl	%eax, %eax
	sete	%bl
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	testb	%bl, %bl
	je	.L3
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-33(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB3:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE3:
	leaq	-48(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB4:
	call	_ZN15WithReturnValue10createFileESs
.LEHE4:
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L3:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-17(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movl	$.LC1, %esi
	movq	%rax, %rdi
.LEHB5:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE5:
	leaq	-32(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB6:
	call	_ZN15WithReturnValue9writeFileESs
.LEHE6:
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN15WithReturnValueD1Ev
	jmp	.L17
.L12:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L5
.L11:
	movq	%rax, %rbx
.L5:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	jmp	.L6
.L14:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L8
.L13:
	movq	%rax, %rbx
.L8:
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	jmp	.L6
.L16:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L10
.L15:
	movq	%rax, %rbx
.L10:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L6:
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN15WithReturnValueD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB7:
	call	_Unwind_Resume
.LEHE7:
.L17:
	addq	$600, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1321:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1321:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1321-.LLSDACSB1321
.LLSDACSB1321:
	.uleb128 .LEHB0-.LFB1321
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB1321
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L11-.LFB1321
	.uleb128 0
	.uleb128 .LEHB2-.LFB1321
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L12-.LFB1321
	.uleb128 0
	.uleb128 .LEHB3-.LFB1321
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L13-.LFB1321
	.uleb128 0
	.uleb128 .LEHB4-.LFB1321
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L14-.LFB1321
	.uleb128 0
	.uleb128 .LEHB5-.LFB1321
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L15-.LFB1321
	.uleb128 0
	.uleb128 .LEHB6-.LFB1321
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L16-.LFB1321
	.uleb128 0
	.uleb128 .LEHB7-.LFB1321
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
.LLSDACSE1321:
	.text
	.size	_Z4testv, .-_Z4testv
	.globl	main
	.type	main, @function
main:
.LFB1325:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z4testv
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.size	main, .-main
	.section	.rodata
	.align 8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
