	.file	"WithReturnValue.cpp"
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZStorSt13_Ios_OpenmodeS_,"axG",@progbits,_ZStorSt13_Ios_OpenmodeS_,comdat
	.weak	_ZStorSt13_Ios_OpenmodeS_
	.type	_ZStorSt13_Ios_OpenmodeS_, @function
_ZStorSt13_Ios_OpenmodeS_:
.LFB872:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %edx
	movl	-8(%rbp), %eax
	orl	%edx, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE872:
	.size	_ZStorSt13_Ios_OpenmodeS_, .-_ZStorSt13_Ios_OpenmodeS_
	.text
	.align 2
	.globl	_ZN15WithReturnValueC2Ev
	.type	_ZN15WithReturnValueC2Ev, @function
_ZN15WithReturnValueC2Ev:
.LFB1322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV15WithReturnValue+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEEC1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	_ZN15WithReturnValueC2Ev, .-_ZN15WithReturnValueC2Ev
	.globl	_ZN15WithReturnValueC1Ev
	.set	_ZN15WithReturnValueC1Ev,_ZN15WithReturnValueC2Ev
	.align 2
	.globl	_ZN15WithReturnValueD2Ev
	.type	_ZN15WithReturnValueD2Ev, @function
_ZN15WithReturnValueD2Ev:
.LFB1325:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1325
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV15WithReturnValue+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	testb	%al, %al
	je	.L5
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE5closeEv
.L5:
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEED1Ev
	movl	$0, %eax
	testl	%eax, %eax
	je	.L4
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
.L4:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1325:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1325-.LLSDACSB1325
.LLSDACSB1325:
.LLSDACSE1325:
	.text
	.size	_ZN15WithReturnValueD2Ev, .-_ZN15WithReturnValueD2Ev
	.globl	_ZN15WithReturnValueD1Ev
	.set	_ZN15WithReturnValueD1Ev,_ZN15WithReturnValueD2Ev
	.align 2
	.globl	_ZN15WithReturnValueD0Ev
	.type	_ZN15WithReturnValueD0Ev, @function
_ZN15WithReturnValueD0Ev:
.LFB1327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN15WithReturnValueD2Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	_ZN15WithReturnValueD0Ev, .-_ZN15WithReturnValueD0Ev
	.align 2
	.globl	_ZN15WithReturnValue8openFileESs
	.type	_ZN15WithReturnValue8openFileESs, @function
_ZN15WithReturnValue8openFileESs:
.LFB1328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	$16, %esi
	movl	$8, %edi
	call	_ZStorSt13_Ios_OpenmodeS_
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE4openERKSsSt13_Ios_Openmode
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1328:
	.size	_ZN15WithReturnValue8openFileESs, .-_ZN15WithReturnValue8openFileESs
	.align 2
	.globl	_ZN15WithReturnValue9checkFileEv
	.type	_ZN15WithReturnValue9checkFileEv, @function
_ZN15WithReturnValue9checkFileEv:
.LFB1329:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1329:
	.size	_ZN15WithReturnValue9checkFileEv, .-_ZN15WithReturnValue9checkFileEv
	.align 2
	.globl	_ZN15WithReturnValue10createFileESs
	.type	_ZN15WithReturnValue10createFileESs, @function
_ZN15WithReturnValue10createFileESs:
.LFB1330:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	-16(%rbp), %rax
	movl	$16, %edx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE4openERKSsSt13_Ios_Openmode
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1330:
	.size	_ZN15WithReturnValue10createFileESs, .-_ZN15WithReturnValue10createFileESs
	.align 2
	.globl	_ZN15WithReturnValue9writeFileESs
	.type	_ZN15WithReturnValue9writeFileESs, @function
_ZN15WithReturnValue9writeFileESs:
.LFB1331:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	leaq	24(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movq	-8(%rbp), %rax
	addq	$272, %rax
	movq	%rax, %rdi
	call	_ZNKSt9basic_iosIcSt11char_traitsIcEE4failEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1331:
	.size	_ZN15WithReturnValue9writeFileESs, .-_ZN15WithReturnValue9writeFileESs
	.weak	_ZTV15WithReturnValue
	.section	.rodata._ZTV15WithReturnValue,"aG",@progbits,_ZTV15WithReturnValue,comdat
	.align 32
	.type	_ZTV15WithReturnValue, @object
	.size	_ZTV15WithReturnValue, 32
_ZTV15WithReturnValue:
	.quad	0
	.quad	_ZTI15WithReturnValue
	.quad	_ZN15WithReturnValueD1Ev
	.quad	_ZN15WithReturnValueD0Ev
	.weak	_ZTI15WithReturnValue
	.section	.rodata._ZTI15WithReturnValue,"aG",@progbits,_ZTI15WithReturnValue,comdat
	.align 16
	.type	_ZTI15WithReturnValue, @object
	.size	_ZTI15WithReturnValue, 16
_ZTI15WithReturnValue:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS15WithReturnValue
	.weak	_ZTS15WithReturnValue
	.section	.rodata._ZTS15WithReturnValue,"aG",@progbits,_ZTS15WithReturnValue,comdat
	.align 16
	.type	_ZTS15WithReturnValue, @object
	.size	_ZTS15WithReturnValue, 18
_ZTS15WithReturnValue:
	.string	"15WithReturnValue"
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
