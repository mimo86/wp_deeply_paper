	.file	"WithExceptions.cpp"
	.section	.text._ZNSt9exceptionC2Ev,"axG",@progbits,_ZNSt9exceptionC5Ev,comdat
	.align 2
	.weak	_ZNSt9exceptionC2Ev
	.type	_ZNSt9exceptionC2Ev, @function
_ZNSt9exceptionC2Ev:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTVSt9exception+16, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	_ZNSt9exceptionC2Ev, .-_ZNSt9exceptionC2Ev
	.weak	_ZNSt9exceptionC1Ev
	.set	_ZNSt9exceptionC1Ev,_ZNSt9exceptionC2Ev
	.section	.rodata
	.type	_ZStL19piecewise_construct, @object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.zero	1
	.section	.text._ZStorSt13_Ios_OpenmodeS_,"axG",@progbits,_ZStorSt13_Ios_OpenmodeS_,comdat
	.weak	_ZStorSt13_Ios_OpenmodeS_
	.type	_ZStorSt13_Ios_OpenmodeS_, @function
_ZStorSt13_Ios_OpenmodeS_:
.LFB872:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %edx
	movl	-8(%rbp), %eax
	orl	%edx, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE872:
	.size	_ZStorSt13_Ios_OpenmodeS_, .-_ZStorSt13_Ios_OpenmodeS_
	.text
	.align 2
	.globl	_ZN14WithExceptionsC2Ev
	.type	_ZN14WithExceptionsC2Ev, @function
_ZN14WithExceptionsC2Ev:
.LFB1322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV14WithExceptions+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEEC1Ev
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1322:
	.size	_ZN14WithExceptionsC2Ev, .-_ZN14WithExceptionsC2Ev
	.globl	_ZN14WithExceptionsC1Ev
	.set	_ZN14WithExceptionsC1Ev,_ZN14WithExceptionsC2Ev
	.align 2
	.globl	_ZN14WithExceptionsD2Ev
	.type	_ZN14WithExceptionsD2Ev, @function
_ZN14WithExceptionsD2Ev:
.LFB1325:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1325
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	$_ZTV14WithExceptions+16, (%rax)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	testb	%al, %al
	je	.L6
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE5closeEv
.L6:
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEED1Ev
	movl	$0, %eax
	testl	%eax, %eax
	je	.L5
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
.L5:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1325:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1325:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1325-.LLSDACSB1325
.LLSDACSB1325:
.LLSDACSE1325:
	.text
	.size	_ZN14WithExceptionsD2Ev, .-_ZN14WithExceptionsD2Ev
	.globl	_ZN14WithExceptionsD1Ev
	.set	_ZN14WithExceptionsD1Ev,_ZN14WithExceptionsD2Ev
	.align 2
	.globl	_ZN14WithExceptionsD0Ev
	.type	_ZN14WithExceptionsD0Ev, @function
_ZN14WithExceptionsD0Ev:
.LFB1327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN14WithExceptionsD2Ev
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	_ZdlPv
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1327:
	.size	_ZN14WithExceptionsD0Ev, .-_ZN14WithExceptionsD0Ev
	.align 2
	.globl	_ZN14WithExceptions8openFileESs
	.type	_ZN14WithExceptions8openFileESs, @function
_ZN14WithExceptions8openFileESs:
.LFB1328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$16, %esi
	movl	$8, %edi
	call	_ZStorSt13_Ios_OpenmodeS_
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	-32(%rbp), %rax
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE4openERKSsSt13_Ios_Openmode
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	xorl	$1, %eax
	testb	%al, %al
	je	.L11
	movl	$8, %edi
	call	__cxa_allocate_exception
	movq	%rax, %rbx
	movq	%rbx, %rdi
	call	_ZNSt9exceptionC1Ev
	movl	$_ZNSt9exceptionD1Ev, %edx
	movl	$_ZTISt9exception, %esi
	movq	%rbx, %rdi
	call	__cxa_throw
.L11:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1328:
	.size	_ZN14WithExceptions8openFileESs, .-_ZN14WithExceptions8openFileESs
	.align 2
	.globl	_ZN14WithExceptions9checkFileEv
	.type	_ZN14WithExceptions9checkFileEv, @function
_ZN14WithExceptions9checkFileEv:
.LFB1332:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1332:
	.size	_ZN14WithExceptions9checkFileEv, .-_ZN14WithExceptions9checkFileEv
	.align 2
	.globl	_ZN14WithExceptions10createFileESs
	.type	_ZN14WithExceptions10createFileESs, @function
_ZN14WithExceptions10createFileESs:
.LFB1333:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	-16(%rbp), %rax
	movl	$16, %edx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE4openERKSsSt13_Ios_Openmode
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSt13basic_fstreamIcSt11char_traitsIcEE7is_openEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1333:
	.size	_ZN14WithExceptions10createFileESs, .-_ZN14WithExceptions10createFileESs
	.align 2
	.globl	_ZN14WithExceptions9writeFileESs
	.type	_ZN14WithExceptions9writeFileESs, @function
_ZN14WithExceptions9writeFileESs:
.LFB1334:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	leaq	24(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKSbIS4_S5_T1_E
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	%rax, %rdi
	call	_ZNSolsEPFRSoS_E
	movq	-8(%rbp), %rax
	addq	$272, %rax
	movq	%rax, %rdi
	call	_ZNKSt9basic_iosIcSt11char_traitsIcEE4failEv
	movzbl	%al, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1334:
	.size	_ZN14WithExceptions9writeFileESs, .-_ZN14WithExceptions9writeFileESs
	.weak	_ZTV14WithExceptions
	.section	.rodata._ZTV14WithExceptions,"aG",@progbits,_ZTV14WithExceptions,comdat
	.align 32
	.type	_ZTV14WithExceptions, @object
	.size	_ZTV14WithExceptions, 32
_ZTV14WithExceptions:
	.quad	0
	.quad	_ZTI14WithExceptions
	.quad	_ZN14WithExceptionsD1Ev
	.quad	_ZN14WithExceptionsD0Ev
	.weak	_ZTI14WithExceptions
	.section	.rodata._ZTI14WithExceptions,"aG",@progbits,_ZTI14WithExceptions,comdat
	.align 16
	.type	_ZTI14WithExceptions, @object
	.size	_ZTI14WithExceptions, 16
_ZTI14WithExceptions:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS14WithExceptions
	.weak	_ZTS14WithExceptions
	.section	.rodata._ZTS14WithExceptions,"aG",@progbits,_ZTS14WithExceptions,comdat
	.align 16
	.type	_ZTS14WithExceptions, @object
	.size	_ZTS14WithExceptions, 17
_ZTS14WithExceptions:
	.string	"14WithExceptions"
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
