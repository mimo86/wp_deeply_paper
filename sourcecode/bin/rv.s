	.file	"mainWithReturnValue.cpp"
	.section	.rodata
.LC0:
	.string	"logfile_rv.log"
.LC1:
	.string	"test_entry"
	.text
	.globl	_Z4testv
	.type	_Z4testv, @function
_Z4testv:
.LFB1084:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA1084
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$600, %rsp
	.cfi_offset 3, -24
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
.LEHB0:
	call	_ZN15WithReturnValueC1Ev
.LEHE0:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-49(%rbp), %rdx
	leaq	-64(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB1:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE1:
	leaq	-64(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB2:
	call	_ZN15WithReturnValue8openFileESs
.LEHE2:
	testl	%eax, %eax
	sete	%bl
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
.LEHB3:
	call	_ZNSsD1Ev
.LEHE3:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	testb	%bl, %bl
	je	.L2
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-33(%rbp), %rdx
	leaq	-48(%rbp), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
.LEHB4:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE4:
	leaq	-48(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB5:
	call	_ZN15WithReturnValue10createFileESs
.LEHE5:
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
.LEHB6:
	call	_ZNSsD1Ev
.LEHE6:
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L2:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcEC1Ev
	leaq	-17(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movl	$.LC1, %esi
	movq	%rax, %rdi
.LEHB7:
	call	_ZNSsC1EPKcRKSaIcE
.LEHE7:
	leaq	-32(%rbp), %rdx
	leaq	-608(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB8:
	call	_ZN15WithReturnValue9writeFileESs
.LEHE8:
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
.LEHB9:
	call	_ZNSsD1Ev
.LEHE9:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
.LEHB10:
	call	_ZN15WithReturnValueD1Ev
.LEHE10:
	jmp	.L16
.L11:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L4
.L10:
	movq	%rax, %rbx
.L4:
	leaq	-49(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	jmp	.L5
.L13:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L7
.L12:
	movq	%rax, %rbx
.L7:
	leaq	-33(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
	jmp	.L5
.L15:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	jmp	.L9
.L14:
	movq	%rax, %rbx
.L9:
	leaq	-17(%rbp), %rax
	movq	%rax, %rdi
	call	_ZNSaIcED1Ev
.L5:
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN15WithReturnValueD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB11:
	call	_Unwind_Resume
.LEHE11:
.L16:
	addq	$600, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1084:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA1084:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1084-.LLSDACSB1084
.LLSDACSB1084:
	.uleb128 .LEHB0-.LFB1084
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB1084
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L10-.LFB1084
	.uleb128 0
	.uleb128 .LEHB2-.LFB1084
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L11-.LFB1084
	.uleb128 0
	.uleb128 .LEHB3-.LFB1084
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L10-.LFB1084
	.uleb128 0
	.uleb128 .LEHB4-.LFB1084
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L12-.LFB1084
	.uleb128 0
	.uleb128 .LEHB5-.LFB1084
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L13-.LFB1084
	.uleb128 0
	.uleb128 .LEHB6-.LFB1084
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L12-.LFB1084
	.uleb128 0
	.uleb128 .LEHB7-.LFB1084
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L14-.LFB1084
	.uleb128 0
	.uleb128 .LEHB8-.LFB1084
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L15-.LFB1084
	.uleb128 0
	.uleb128 .LEHB9-.LFB1084
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L14-.LFB1084
	.uleb128 0
	.uleb128 .LEHB10-.LFB1084
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB11-.LFB1084
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
.LLSDACSE1084:
	.text
	.size	_Z4testv, .-_Z4testv
	.globl	main
	.type	main, @function
main:
.LFB1085:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z4testv
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1085:
	.size	main, .-main
	.section	.rodata
	.align 8
	.type	_ZZL18__gthread_active_pvE20__gthread_active_ptr, @object
	.size	_ZZL18__gthread_active_pvE20__gthread_active_ptr, 8
_ZZL18__gthread_active_pvE20__gthread_active_ptr:
	.quad	_ZL28__gthrw___pthread_key_createPjPFvPvE
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
