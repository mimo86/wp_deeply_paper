#!/bin/bash

echo
echo RV with:
for i in `seq 1 1 10`
do
    rm logfile_rv.log
    ./bin/rv_time
done
echo RV ohne:
for i in `seq 1 1 10`
do 
    ./bin/rv_time
done

echo
echo EX with:
for i in `seq 1 1 10`
do 
    rm logfile_ex.log
    ./bin/ex_time
done
echo EX ohne:
for i in `seq 1 1 10`
do 
    ./bin/ex_time
done

echo done

# (238 + 205 + 219 + 174 + 158 + 139 + 232 + 180 + 137 + 227 ) / 10
# (140 + 139 + 147 + 113 + 113 + 114 + 96 + 94 + 96 + 89) / 10
# (170 + 154 + 186 + 154 + 221 + 250 + 205 + 188 + 246 + 174 ) / 10
# (151 + 163 + 141 + 133 + 128 + 181 + 166 + 129 + 114 + 155 ) / 10
