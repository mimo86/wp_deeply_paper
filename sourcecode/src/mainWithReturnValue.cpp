#include "WithReturnValue.h"

#define LOGFILENAME "logfile_rv.log"

void test(void)
{
    WithReturnValue wrv;
    if ( ! wrv.openFile(LOGFILENAME) )
    {
        wrv.createFile(LOGFILENAME);
    }
    wrv.writeFile( "test_entry" );
}

int main( void)
{ 
    test();
	return 0;
}
