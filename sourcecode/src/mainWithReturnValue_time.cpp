#include "WithReturnValue.h"
#include <iostream>
#include <chrono>

#define LOGFILENAME "logfile_rv.log"

void test(void)
{
    WithReturnValue wrv;
    if ( ! wrv.openFile(LOGFILENAME) )
    {
        wrv.createFile(LOGFILENAME);
    }
    wrv.writeFile( "test_entry" );
}

int main( void)
{ 
    auto start = std::chrono::high_resolution_clock::now();
    test();
    std::cout<< std::chrono::duration_cast< std::chrono::duration<int, std::micro> >(std::chrono::high_resolution_clock::now() - start ).count() << "\n";
	return 0;
}
