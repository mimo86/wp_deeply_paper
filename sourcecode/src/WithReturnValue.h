#ifndef WITHRETURNVALUE_H_
#define WITHRETURNVALUE_H_

#include <fstream>
#include <string>

class WithReturnValue
{
public:
	virtual ~WithReturnValue();
    int openFile( std::string filename );
    int checkFile( );
    int createFile( std::string filename );
    int writeFile( std::string toWrite );
private:
    std::fstream fd_;
};

#endif

