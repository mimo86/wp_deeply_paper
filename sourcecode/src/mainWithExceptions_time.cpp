#include "WithExceptions.h"
#include <iostream>
#include <chrono>

#define LOGFILENAME "logfile_ex.log"

void test(void)
{
    WithExceptions wex;
    try
    {
        wex.openFile(LOGFILENAME);
    }
    catch (std::exception& e )
    {
        wex.createFile(LOGFILENAME);
    }
    wex.writeFile( "test_entry" );
}

int main( void)
{
    auto start = std::chrono::high_resolution_clock::now();
    test();
    std::cout<< std::chrono::duration_cast< std::chrono::duration<int, std::micro> >(std::chrono::high_resolution_clock::now() - start ).count() << "\n";
	return 0;
}
