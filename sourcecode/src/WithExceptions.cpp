#include "WithExceptions.h"

WithExceptions:: ~WithExceptions()
{
    if (fd_.is_open())
    {
        fd_.close();
    }
}

void WithExceptions::openFile( std::string filename )
{   
    fd_.open( filename );
    if ( ! fd_.is_open() )
    {
        throw std::exception();
    }
}

int WithExceptions::checkFile()
{
    return fd_.is_open();
}

int WithExceptions::createFile( std::string filename )
{
    fd_.open( filename, std::ios::out ); 
    return fd_.is_open();
}

int WithExceptions::writeFile( std::string toWrite )
{
    fd_ << toWrite << std::endl;
    return fd_.fail();
}

