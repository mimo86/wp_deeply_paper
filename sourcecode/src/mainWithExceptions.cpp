#include "WithExceptions.h"

#define LOGFILENAME "logfile_ex.log"

void test(void)
{
    WithExceptions wex;
    try
    {
        wex.openFile(LOGFILENAME);
    }
    catch (std::exception& e )
    {
        wex.createFile(LOGFILENAME);
    }
    wex.writeFile( "test_entry" );
}

int main( void)
{
    test();
	return 0;
}
