#include "WithReturnValue.h"

WithReturnValue:: ~WithReturnValue()
{
    if (fd_.is_open())
    {
        fd_.close();
    }
}

int WithReturnValue::openFile( std::string filename )
{   
    fd_.open( filename );
    return fd_.is_open();
}

int WithReturnValue::checkFile()
{
    return fd_.is_open();
}

int WithReturnValue::createFile( std::string filename )
{
    fd_.open( filename, std::ios::out );
    return fd_.is_open();
}

int WithReturnValue::writeFile( std::string toWrite )
{
    fd_ << toWrite << std::endl;
    return fd_.fail();
}
