#ifndef WITHEXCEPTIONS_H_
#define WITHEXCEPTIONS_H_

#include <fstream>
#include <string>
#include <exception>

class WithExceptions
{
public:
	virtual ~WithExceptions();
    void openFile( std::string filename );
    int checkFile( );
    int createFile( std::string filename );
    int writeFile( std::string toWrite );
private:
    std::fstream fd_;
};

#endif
